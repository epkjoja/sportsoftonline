package se.jojje.online.gui;

import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.trolltech.qt.core.QDate;
import com.trolltech.qt.core.QSettings;
import com.trolltech.qt.core.QTime;
import com.trolltech.qt.core.QTimer;
import com.trolltech.qt.gui.QApplication;
import com.trolltech.qt.gui.QMainWindow;
import com.trolltech.qt.gui.QMessageBox;
import com.trolltech.qt.gui.QMessageBox.Icon;
import com.trolltech.qt.gui.QWidget;

import se.jojje.online.common.Punch;
import se.jojje.online.common.PunchQueue;
import se.jojje.online.input.roc.RocConnector;
import se.jojje.online.output.sportsoft.SpeakerClient;
import se.jojje.online.output.sportsoft.SpeakerClient2003;
import se.jojje.online.output.sportsoft.SpeakerClient2010;

public class SportSoftOnline extends QMainWindow {
	private static final Logger logger = Logger.getLogger(SportSoftOnline.class);

	private static String VERSION;
	private static String APPLICATION;
	private static String ORGANIZATION;

	private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static final int SecsBetweenReconnects = 5;

	private final Ui_SportSoftOnline ui = new Ui_SportSoftOnline();
	private SpeakerClient oeClient = null;
	// private SiGprsConnector server = null;
	private RocConnector rocClient = null;
	private PunchQueue punchQ = null;
	private final QTimer timer = new QTimer(this);
	private QTime timeLastFetched = QTime.currentTime();

	private Punch savedPunch = null;

	private boolean rocConnectionDown;

	public static void main(String[] args) {
		QApplication.initialize(args);

		readProperties();

		SportSoftOnline ps = new SportSoftOnline();
		ps.show();

		ps.restoreSettings();
		QApplication.exec();
		ps.saveSettings();
	}

	private static void readProperties() {
		Properties prop = new Properties();
		try {
			// load a properties file from class path
			prop.load(SportSoftOnline.class.getClassLoader().getResourceAsStream("SportSoftOnline.properties"));

			// get the property value
			VERSION = prop.getProperty("version");
			APPLICATION = prop.getProperty("application");
			ORGANIZATION = prop.getProperty("organization");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SportSoftOnline() {
		ui.setupUi(this);
		setupDefaults();
		setupConnects();
	}

	public SportSoftOnline(QWidget parent) {
		super(parent);
		ui.setupUi(this);
	}

	private void saveSettings() {
		QSettings settings = new QSettings(ORGANIZATION, APPLICATION);

		settings.setValue("rocUrl", ui.rocUrl.currentText());
		settings.setValue("rocMac", ui.rocMac.text());
		settings.setValue("rocPollInterval", ui.rocPollInterval.value());
		settings.setValue("rocPacket", ui.rocFromPacket.text());
		settings.setValue("speakerHost", ui.speakerHost.text());
		settings.setValue("oe_competition", ui.oe_competition.value());
		settings.setValue("zeroTime/hour", ui.zeroTime.time().hour());
		settings.setValue("zeroTime/minute", ui.zeroTime.time().minute());
	}

	private void restoreSettings() {
		QSettings settings = new QSettings(ORGANIZATION, APPLICATION);

		ui.rocUrl.setEditText(settings.value("rocUrl", "http://roc.olresultat.se/ver3.2/getpunches.asp").toString());
		ui.rocMac.setText(settings.value("rocMac", "").toString());
		ui.rocPollInterval.setValue(Integer.parseInt(settings.value("rocPollInterval", 30).toString()));
		ui.rocFromPacket.setValue(Integer.parseInt(settings.value("rocPacket", 0).toString()));

		ui.speakerHost.setText(settings.value("speakerHost", "localhost").toString());
		ui.oe_competition.setValue(Integer.parseInt(settings.value("oe_competition", 1).toString()));

		int hour = Integer.parseInt(settings.value("zeroTime/hour", 10).toString());
		int minute = Integer.parseInt(settings.value("zeroTime/minute", 0).toString());
		ui.zeroTime.setTime(new QTime(hour, minute));
	}

	private void setupDefaults() {
		this.setWindowTitle(APPLICATION + " - " + VERSION);

		ui.stopButton.setDisabled(true);
		ui.rocPollButton.setDisabled(true);

		ui.rocFromDate.setDate(QDate.currentDate());
		ui.rocFromTime.setTime(new QTime(0, 0, 0));
	}

	private void setupConnects() {
		ui.startButton.clicked.connect(this, "start()");
		ui.stopButton.clicked.connect(this, "stop()");
		ui.exitButton.clicked.connect(QApplication.instance(), "quit()");
		ui.rocPollButton.clicked.connect(this, "pollPunchServer()");

		timer.timeout.connect(this, "timerTimeout()");
	}

	public void start() {
		if (rocClient != null)
			rocClient = null;

		if (oeClient != null)
			oeClient.stop();

		if (punchQ == null)
			punchQ = new PunchQueue();

		try {
			if (ui.oe_competition.value() == 0) {
				String hostName = "ROC - " + InetAddress.getLocalHost().getHostName();
				oeClient = new SpeakerClient2010(ui.speakerHost.text(), hostName);
			} else {
				int zeroTimeSec = ui.zeroTime.time().hour() * 3600 + ui.zeroTime.time().minute() * 60;
				oeClient = new SpeakerClient2003(ui.speakerHost.text(), ui.oe_competition.value() + 10000, zeroTimeSec);
			}
			oeClient.start();
		} catch (Exception e) {
			QMessageBox msg = new QMessageBox(Icon.Warning, "Connection error",
					"Could not connect to SportSoftware Speaker server!\nCheck the configuation and try again.\n" + e.getMessage());
			msg.exec();
			return;
		}

		// server = new SiGprsConnector(q, 0);
		rocClient = new RocConnector(ui.rocUrl.currentText(), ui.rocMac.text(), ui.rocFromDate.text(), ui.rocFromTime.text(), ui.rocFromPacket.value());
		rocClient.newPunch.connect(this, "newPunch(Punch)");

		if (!rocClient.checkUrl()) {
			QMessageBox msg = new QMessageBox(Icon.Warning, "Connection error",
					"Failed to connect to the ROC web service!\nCheck the configuration and try again.");
			msg.exec();

			oeClient.stop();
			return;
		}

		ui.output.appendHtml(new SimpleDateFormat("HH:mm:ss").format(new Date()) + "\t<font color='green'>Connection to Speaker server established.</font>");
		ui.statusbar.showMessage("Server started", 10000);

		// Start the timer with 1 sec's timeouts
		timer.start(1000);

		ui.rocUrl.setDisabled(true);
		ui.rocMac.setDisabled(true);
		ui.rocFromDate.setDisabled(true);
		ui.rocFromTime.setDisabled(true);
		ui.rocFromPacket.setDisabled(true);
		ui.speakerHost.setDisabled(true);
		ui.startButton.setDisabled(true);
		ui.exitButton.setDisabled(true);
		ui.oe_competition.setDisabled(true);
		ui.zeroTime.setDisabled(true);

		ui.stopButton.setEnabled(true);
		ui.rocPollButton.setEnabled(true);
	}

	public void stop() throws IOException {
		timer.stop();

		if (oeClient != null) {
			oeClient.stop();
			oeClient = null;
		}

		rocClient = null;

		ui.output.appendHtml(new SimpleDateFormat("HH:mm:ss").format(new Date()) + "\t<font color='green'>Disconnected from Speaker server.</font>");
		ui.statusbar.showMessage("Server stopped", 10000);

		ui.rocUrl.setEnabled(true);
		ui.rocMac.setEnabled(true);
		ui.rocFromDate.setEnabled(true);
		ui.rocFromTime.setEnabled(true);
		ui.rocFromPacket.setEnabled(true);
		ui.speakerHost.setEnabled(true);
		ui.startButton.setEnabled(true);
		ui.exitButton.setEnabled(true);
		ui.oe_competition.setEnabled(true);
		ui.zeroTime.setEnabled(true);

		ui.stopButton.setDisabled(true);
		ui.rocPollButton.setDisabled(true);

	}

	public boolean pollPunchServer() {
		if (rocClient.poll() < 0) {
			if (!rocConnectionDown) {
				StringBuilder sb = new StringBuilder();
				sb.append(timeFormat.format(new Date())).append("\t<font color='red'>Failed to fetch punches from web service!</font>");
				ui.output.appendHtml(sb.toString());
				rocConnectionDown = true;
			}
		} else if (rocConnectionDown) {
			StringBuilder sb = new StringBuilder();
			sb.append(timeFormat.format(new Date())).append("\t<font color='red'>Web service OK again.</font>");
			ui.output.appendHtml(sb.toString());
			rocConnectionDown = false;
		}

		timeLastFetched = QTime.currentTime();
		ui.rocFromPacket.setValue(rocClient.getLastPacket());

		return true;
	}

	private void handleReconnects() {
		if (oeClient.isConnectFailure()) {
			try {
				oeClient.start();

				StringBuilder sb = new StringBuilder();
				sb.append(timeFormat.format(new Date())).append("\t<font color='red'>Connection OK to Speaker server again.</font>");
				ui.output.appendHtml(sb.toString());

				while (punchQ.hasMorePunches()) {
					Punch p = punchQ.peek();
					if (oeClient.sendAPunch(p)) {
						logger.debug("Failed to resend!");
						break;
					} else {
						// Punch has been sent. Remove it from Q
						punchQ.getNextPunch();
					}
				}
			} catch (IOException e) {
				logger.debug("Reconnect failed!");
			}
		}
	}

	/**
	 * Timer timeout handling
	 */
	@SuppressWarnings("unused")
	private void timerTimeout() {
		int timeLeft;
		QTime now = QTime.currentTime();

		if (now.second() % SecsBetweenReconnects == 0) {
			handleReconnects();
		}

		if (now.compareTo(timeLastFetched) < 0) {
			// We have probably past midnight or started DST
			timeLeft = -1;
		} else {
			timeLeft = now.secsTo(timeLastFetched) + ui.rocPollInterval.value();
		}

		if (timeLeft < 0) {
			// There was more than pollTime seconds since we polled last time
			pollPunchServer();
		} else {
			ui.statusbar.showMessage("Running: " + timeLeft + " seconds to next poll.", ui.rocPollInterval.value() * 1000);
		}
	}

	@SuppressWarnings("unused")
	private void newPunch(Punch p) {
		StringBuilder sb = new StringBuilder();
		sb.append(timeFormat.format(new Date())).append("\tSI: ").append(p.getCardNumber()).append("\tControl: ").append(p.getControlCode())
				.append("\tPunchtime: ").append(timeFormat.format(p.getAbsPunchTime()));
		ui.output.appendPlainText(sb.toString());

		if (punchQ.getNbrOfPunches() > 0) {
			// If there still are queued punches put the new punch last in queue
			punchQ.punch(p);
		} else if (oeClient.sendAPunch(p)) {
			// Send failed!
			sb.setLength(0);
			sb.append(timeFormat.format(new Date())).append("\t<font color='red'>Lost connection to Speaker server!</font>");
			ui.output.appendHtml(sb.toString());

			if (savedPunch != null) {
				punchQ.punch(savedPunch);
			}
			punchQ.punch(p);
		} else {
			// Send seemed to succeed. Save the last punch if the next one
			// fails.
			savedPunch = p;
		}
	}
}
