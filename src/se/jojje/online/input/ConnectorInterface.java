package se.jojje.online.input;


public interface ConnectorInterface {

	int poll();

	String getType();
}
