package se.jojje.online.input.sigprs;

import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import se.jojje.online.common.Punch;
import se.jojje.online.common.PunchQueue;
import se.jojje.online.input.ConnectorInterface;

public class SiGprsConnector implements ConnectorInterface {

	private static final Logger logger = Logger.getLogger(SiGprsConnector.class);

	private static final Pattern punchPattern = Pattern.compile("^(\\d+);(.*)$", Pattern.MULTILINE);
	// private static final SimpleDateFormat sdf = new
	// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final PunchQueue q;
	private String url;
	private final int compId;
	private int lastPacket;

	private final StringBuilder statusText = new StringBuilder();

	public SiGprsConnector(PunchQueue que, int compId) {
		this.q = que;
		this.url = "http://www.lok.se/sigprs/getPunches.php";
		this.lastPacket = 0;
		this.compId = compId;
	}

	public SiGprsConnector(PunchQueue que, String serverUrl, int compId, int lastPacket) {
		this(que, compId);
		this.url = serverUrl;
		this.lastPacket = lastPacket;
	}

	@Override
	public int poll() {
		String wholeUrl = url + "?competitionId=" + compId + "&lastId=" + lastPacket;

		String content = "";
		Scanner scanner = null;
		URLConnection connection = null;
		try {
			connection = new URL(wholeUrl).openConnection();
			scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\Z");
			content = scanner.next();
		} catch (NoSuchElementException elemEx) {
			System.out.println("NO DATA");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			scanner.close();
		}

		logger.debug("Data read: " + content);
		// System.out.println(content);

		List<Punch> punches = parsePunches(content);

		int size = punches.size();

		for (Punch p : punches) {
			q.punch(p);
		}

		return size;
	}

	private List<Punch> parsePunches(String content) {
		List<Punch> toRet = new ArrayList<Punch>();

		if (content.isEmpty())
			return toRet;

		Matcher matcher = punchPattern.matcher(content);

		while (matcher.find()) {
			lastPacket = Integer.parseInt(matcher.group(1));
			String data = matcher.group(2);

			if (data.length() != 26) {
				logger.debug("Error in the data!");
			}

			byte[] punch = DatatypeConverter.parseHexBinary(data);

			int code = punch[4] & 0xff;
			int card = createInt((byte) 0, punch[5], punch[6], punch[7]);
			int time = createInt((byte) 0, (byte) 0, punch[8], punch[9]);
			int twd = punch[10] & 0xff;

			int absPunchTime = time + ((twd & 1) * 3600 * 12);

			System.out.println("Punch: " + lastPacket + " " + code + " " + card + " " + absPunchTime);

			toRet.add(new Punch(code, card, absPunchTime));

			statusText.append(new SimpleDateFormat("HH:mm:ss").format(new Date())).append("\tPacket: ").append(lastPacket).append("\tCard: ").append(card)
					.append("\tCode: ").append(code).append("\tPunchtime: ").append(time).append("\tTWD: ").append(twd).append("\n");
		}

		return toRet;
	}

	private int createInt(byte b1, byte b2, byte b3, byte b4) {
		return ((b1 & 0xff) << 24) + ((b2 & 0xff) << 16) + ((b3 & 0xff) << 8) + (b4 & 0xff);
	}

	public String getStatusText() {
		String ret = statusText.toString();
		statusText.setLength(0);

		if (ret.length() > 0)
			ret = ret.substring(0, ret.length() - 1);

		return ret;
	}

	public int getLastPacket() {
		return lastPacket;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

}
