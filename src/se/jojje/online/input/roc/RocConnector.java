package se.jojje.online.input.roc;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import se.jojje.online.common.Punch;
import se.jojje.online.common.PunchProducer;
import se.jojje.online.input.ConnectorInterface;

public class RocConnector extends PunchProducer implements ConnectorInterface {
	private static final Logger logger = Logger.getLogger(RocConnector.class);

	private static final Pattern punchPattern = Pattern.compile("^(\\d+);(\\d+);(\\d+);(.*)$", Pattern.MULTILINE);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private String url;
	private final String macAddress;
	private String fromDate;
	private String fromTime;
	private int lastPacket;

	private final StringBuilder statusText = new StringBuilder();

	public RocConnector(String macAddress) {
		this.url = "http://roc.olresultat.se/ver2.1/getpunches.asp";
		this.lastPacket = 0;
		this.macAddress = macAddress;
	}

	public RocConnector(String url, String macAddress, String fromDate, String fromTime, int fromPacket) {
		this(macAddress);
		this.url = url;
		this.fromDate = fromDate;
		this.fromTime = fromTime;
		this.lastPacket = fromPacket;
	}

	public boolean checkUrl() {
		try {
			String wholeUrl = url + "?unitId=" + macAddress + "&lastId=" + lastPacket;
			HttpURLConnection connection = (HttpURLConnection) new URL(wholeUrl).openConnection();
			connection.setRequestMethod("HEAD");
			connection.connect();

			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				logger.debug("The URL is not correct!");
				return false;
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	public int poll() {
		String wholeUrl = url + "?unitId=" + macAddress + "&lastId=" + lastPacket;
		logger.debug(wholeUrl);

		String content = null;
		Scanner scanner = null;
		URLConnection connection = null;
		try {
			connection = new URL(wholeUrl).openConnection();
			scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\Z");
			content = scanner.next();
		} catch (NoSuchElementException ex) {
			logger.debug("No data returned");
			return 0;
		} catch (UnknownHostException ex) {
			logger.debug("Failed to connect to Roc server!");
			return -1;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}

		logger.debug("Data read: " + content);

		List<Punch> punches = parsePunches(content);

		int size = punches.size();

		for (Punch p : punches) {
			punch(p);
		}

		return size;
	}

	private List<Punch> parsePunches(String content) {
		List<Punch> toRet = new ArrayList<Punch>();

		Matcher matcher = punchPattern.matcher(content);

		while (matcher.find()) {
			lastPacket = Integer.parseInt(matcher.group(1));
			int code = Integer.parseInt(matcher.group(2));
			int card = Integer.parseInt(matcher.group(3));
			String dateStr = matcher.group(4);

			Date date = null;
			try {
				date = sdf.parse(dateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			logger.debug("RocPunch: " + lastPacket + ", " + code + ", " + card + ", " + date);

			toRet.add(new Punch(code, card, date));
		}

		return toRet;
	}

	public String getStatusText() {
		String ret = statusText.toString();
		statusText.setLength(0);

		if (ret.length() > 0)
			ret = ret.substring(0, ret.length() - 1);

		return ret;
	}

	public int getLastPacket() {
		return lastPacket;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
}
