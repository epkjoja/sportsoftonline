package se.jojje.online.common;

public interface PunchConsumer {
	Punch getNextPunch() throws InterruptedException;
	
	int getNbrOfPunches();

	boolean hasMorePunches();
}
