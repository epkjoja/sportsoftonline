package se.jojje.online.common;

import com.trolltech.qt.QSignalEmitter;

public abstract class PunchProducer extends QSignalEmitter {

	public Signal1<Punch> newPunch = new Signal1<Punch>();

	protected void punch(Punch punch) {
		newPunch.emit(punch);
	}
}
