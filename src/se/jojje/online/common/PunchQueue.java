package se.jojje.online.common;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

public class PunchQueue {
	private static final Logger logger = Logger.getLogger(PunchQueue.class);

	private final BlockingQueue<Punch> que;

	public PunchQueue() {
		que = new LinkedBlockingQueue<Punch>();
	}

	public void punch(Punch punch) {
		que.add(punch);
		logger.debug("Add to Q, size is: " + que.size());
	}

	public Punch peek() {
		logger.debug("Peek in Q, size is: " + que.size());
		return que.peek();
	}

	public Punch getNextPunch() {
		logger.debug("GetNextPunch from Q, size before is: " + que.size());
		return que.poll();
	}

	public int getNbrOfPunches() {
		logger.debug("GetNbrOfPunches from Q, size is: " + que.size());
		return que.size();
	}

	public boolean hasMorePunches() {
		return !que.isEmpty();
	}

}
