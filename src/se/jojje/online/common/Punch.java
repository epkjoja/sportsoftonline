package se.jojje.online.common;

import java.util.Calendar;
import java.util.Date;

public class Punch {
	private final int controlCode;
	private final int cardNumber;
	private Date absPunchTime;

	private int punchSource;

	public Punch(int controlCode, int cardNumber, int absPunchTime) {
		this.controlCode = controlCode;
		this.cardNumber = cardNumber;
		// this.absPunchTime = absPunchTime;
	}

	public Punch(int controlCode, int cardNumber, Date absPunchTime) {
		this.controlCode = controlCode;
		this.cardNumber = cardNumber;
		this.absPunchTime = absPunchTime;
	}

	public int getControlCode() {
		return controlCode;
	}

	public int getCardNumber() {
		return cardNumber;
	}

	public Date getAbsPunchTime() {
		return absPunchTime;
	}

	public int getRelPunchTime(int zeroTimeSecs) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(absPunchTime);

		int secs = cal.get(Calendar.HOUR_OF_DAY) * 3600;
		secs += cal.get(Calendar.MINUTE) * 60;
		secs += cal.get(Calendar.SECOND);

		int relSecs = secs - zeroTimeSecs;

		if (relSecs < 0) {
			relSecs += 24 * 60 * 60;
		}

		return relSecs;
	}
}
