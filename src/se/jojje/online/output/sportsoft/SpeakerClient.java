package se.jojje.online.output.sportsoft;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import se.jojje.online.common.Punch;

public abstract class SpeakerClient {
    private static final Logger logger = Logger.getLogger(SpeakerClient.class);

    protected final String hostName;
    protected final int port;
    protected Socket socket = null;
    private boolean connectFail = false;

    protected SpeakerClient(String hostName, int port) {
        this.hostName = hostName;
        this.port = port;
    }

    public void start() throws UnknownHostException, IOException {
        socket = new Socket(hostName, port);
        connectFail = false;
    }

    public void stop() {
        try {
            if (socket != null) {
                socket.close();
                socket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a punch to the Speaker server
     * 
     * @param punch
     * @return true - could not send punch, false - send OK
     */
    public boolean sendAPunch(Punch punch) {
        if (socket == null) {
            try {
                start();
            } catch (IOException e) {
                connectFail = true;
                stop();
                return connectFail;
            }
        }

        byte[] data = buildPunch(punch);

        try {
            OutputStream os = socket.getOutputStream();
            // There is a problem here but there seem to be no way of fixing it.
            // The first 'write' after the server has closed the socket will
            // always succeed. The second write will get the SocketException.
            os.write(data, 0, data.length);
        } catch (IOException e) {
            // If we fail to send a punch, just close the socket and reconnect
            // on the next send
            connectFail = true;
            stop();
        }

        return connectFail;
    }

    protected abstract byte[] buildPunch(Punch punch);

    public boolean isConnectFailure() {
        return connectFail;
    }
}
