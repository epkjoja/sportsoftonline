package se.jojje.online.output.sportsoft;

import org.apache.log4j.Logger;

import se.jojje.online.common.Punch;

public class SpeakerClient2003 extends SpeakerClient {
    private static final Logger logger = Logger.getLogger(SpeakerClient2003.class);

    private final int zeroTime;

    public SpeakerClient2003(String hostName, int port, int zeroTime) {
        super(hostName, port);
        this.zeroTime = zeroTime;
    }

    /**
     * Build the raw data needed for sending a punch to OE/OS/MT 2003
     * 
     * @param punch
     * @return
     */
    protected byte[] buildPunch(Punch punch) {
        byte[] data = new byte[15];

        data[0] = 0;

        // Kontrollnummer, 2 bytes
        data[1] = (byte) (punch.getControlCode() & 0xff);
        data[2] = 0;

        // Bricknummer, 4 bytes
        data[3] = (byte) (punch.getCardNumber() & 0xff);
        data[4] = (byte) (punch.getCardNumber() >> 8 & 0xff);
        data[5] = (byte) (punch.getCardNumber() >> 16 & 0xff);
        data[6] = (byte) (punch.getCardNumber() >> 24 & 0xff);

        // Ok�nd betydelse, 4 bytes
        data[7] = 0;
        data[8] = 0;
        data[9] = 0;
        data[10] = 0;

        // Tid i tiondels sekunder, 4 bytes
        int time = punch.getRelPunchTime(zeroTime) * 10;
        logger.debug("ZeroTime: " + zeroTime + ", Time: " + time);

        data[11] = (byte) (time & 0xff);
        data[12] = (byte) (time >> 8 & 0xff);
        data[13] = (byte) (time >> 16 & 0xff);
        data[14] = (byte) (time >> 24 & 0xff);

        return data;
    }

    /**
     * Build the raw data needed for sending a punch to OE2010
     * 
     * @param punch
     * @return
     */
    private byte[] buildPunch2010(Punch punch) {
        byte[] data = new byte[36];

        data[0] = 0x07;

        // Datornamn, 21 bytes
        data[1] = 0x4a;
        data[2] = 0x4f;
        data[3] = 0x4a;
        data[4] = 0x41;

        // Kontrollnummer, 2 bytes
        data[22] = (byte) (punch.getControlCode() & 0xff);
        data[23] = 0;

        // Bricknummer, 4 bytes
        data[24] = (byte) (punch.getCardNumber() & 0xff);
        data[25] = (byte) (punch.getCardNumber() >> 8 & 0xff);
        data[26] = (byte) (punch.getCardNumber() >> 16 & 0xff);
        data[27] = (byte) (punch.getCardNumber() >> 24 & 0xff);

        // Ok�nd betydelse, 4 bytes
        data[28] = 0;
        data[29] = 0;
        data[30] = 0;
        data[31] = 0;

        // Absolut tid i hundradels sekunder, 4 bytes
        int time = punch.getRelPunchTime(0) * 100;
        logger.debug("Absolute time: " + time);

        data[32] = (byte) (time & 0xff);
        data[33] = (byte) (time >> 8 & 0xff);
        data[34] = (byte) (time >> 16 & 0xff);
        data[35] = (byte) (time >> 24 & 0xff);

        return data;
    }
}
