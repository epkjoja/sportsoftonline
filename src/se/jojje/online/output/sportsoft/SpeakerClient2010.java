package se.jojje.online.output.sportsoft;

import org.apache.log4j.Logger;

import se.jojje.online.common.Punch;

public class SpeakerClient2010 extends SpeakerClient {
    private static final Logger logger = Logger.getLogger(SpeakerClient2010.class);

    private final String sourceInfo;

    public SpeakerClient2010(String hostName, String sourceInfo) {
        super(hostName, 10000);
        this.sourceInfo = sourceInfo;
    }

    /**
     * Build the raw data needed for sending a punch to OE2010
     * 
     * @param punch
     * @return
     */
    protected byte[] buildPunch(Punch punch) {
        byte[] data = new byte[36];

        data[0] = 0x07;

        // Datornamn, 21 bytes
        logger.debug("Source length: " + sourceInfo.length());
        logger.debug("Bytes: " + sourceInfo.getBytes());
        
        int bytesToCopy = sourceInfo.length() <= 21 ? sourceInfo.length() : 21;
        System.arraycopy(sourceInfo.getBytes(), 0, data, 1, bytesToCopy);

        // Kontrollnummer, 2 bytes
        data[22] = (byte) (punch.getControlCode() & 0xff);
        data[23] = 0;

        // Bricknummer, 4 bytes
        data[24] = (byte) (punch.getCardNumber() & 0xff);
        data[25] = (byte) (punch.getCardNumber() >> 8 & 0xff);
        data[26] = (byte) (punch.getCardNumber() >> 16 & 0xff);
        data[27] = (byte) (punch.getCardNumber() >> 24 & 0xff);

        // Ok�nd betydelse, 4 bytes
        data[28] = 0;
        data[29] = 0;
        data[30] = 0;
        data[31] = 0;

        // Absolut tid i hundradels sekunder, 4 bytes
        int time = punch.getRelPunchTime(0) * 100;
        logger.debug("Absolute time: " + time);

        data[32] = (byte) (time & 0xff);
        data[33] = (byte) (time >> 8 & 0xff);
        data[34] = (byte) (time >> 16 & 0xff);
        data[35] = (byte) (time >> 24 & 0xff);

        // Sleep a little while otherwise OE2010 might crash!
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return data;
    }
}
