package se.jojje.online.output.sportsoft;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Date;

import org.junit.Test;

import se.jojje.online.common.Punch;

public class ClientTest {

	@Test
	public void testClient() throws UnknownHostException, IOException {
		SpeakerClient sc = new SpeakerClient2003("localhost", 10001, 0);
		Punch p = new Punch(50, 36641, new Date());
		sc.sendAPunch(p);
		sc.stop();
	}
}
