package se.jojje.online.input.roc;

import java.io.IOException;

import org.junit.Test;

public class RocTest {

	@Test
	public void testPoll() {
		RocConnector rc = new RocConnector("b827eb4a00ca");
		rc.poll();
	}

	@Test
	public void testCheckUrl() throws IOException {
		RocConnector rc = new RocConnector("http://roc.olresultat.se/ver2/getpunches.as", "b827eb4a00ca", "", "", 0);
		System.out.println(rc.checkUrl());
	}

}
